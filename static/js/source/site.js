$(function(){
    var winterfell = new Winterfell({
        widgets: [
            {
                type: 'video',
                data: {
                    source: '/static/media/sample.mp4',
                    poster: '/static/media/sample.jpg'
                },
                additionalClasses: 'transparent',
                appendTo: '#col-1'
            },
            {
                type: 'revenue',
                data: [
                    {revenue: 3764, inquiries: 12},
                    {revenue: 5871, inquiries: 48},
                    {revenue: 4506, inquiries: 76},
                    {revenue: 9738, inquiries: 35},
                    {revenue: 7284, inquiries: 16},
                    {revenue: 5546, inquiries: 84},
                    {revenue: 6428, inquiries: 53},
                    {revenue: 8964, inquiries: 91},
                    {revenue: 9296, inquiries: 23},
                    {revenue: 7341, inquiries: 35},
                    {revenue: 5297, inquiries: 72},
                    {revenue: 8412, inquiries: 50}
                ],
                appendTo: '#col-1'
            },
            {
                type: 'tasks',
                data: [
                    {name: 'Some Task #1', completed: true, point: [37.6, 55.6]},
                    {name: 'Some Task #2', completed: false, point: [31.601, 55.6]},
                    {name: 'Some Task #3', completed: true, point: [74.86, 37.602]},
                    {name: 'Some Task #4', completed: false, point: [11.601, 45.6]},
                    {name: 'Some Task #5', completed: true, point: [41.601, 65.6]},
                    {name: 'Some Task #6', completed: false, point: [21.601, 15.6]},
                    {name: 'Some Task #7', completed: true, point: [35.601, 45.6]},
                    {name: 'Some Task #8', completed: true, point: [34.86, 10.91]},
                    {name: 'Some Task #9', completed: true, point: [44.86, 30.91]},
                    {name: 'Some Task #10', completed: true, point: [4.86, 45.11]}
                ],
                appendTo: '#col-2'
            },
            {
                type: 'mail',
                appendTo: '#col-2'
            },
            {
                type: 'login',
                additionalClasses: 'transparent',
                appendTo: '#col-3'
            },
            {
                type: 'card',
                data: {
                    userpic: '/static/img/arya.png',
                    name: 'Arya Stark',
                    hometown: 'Winterfell',
                    likes: 2719,
                    friends: 5386,
                    views: 24953
                },
                appendTo: '#col-3'
            },
            {
                type: 'events',
                appendTo: '#col-3',
                additionalClasses: 'transparent'
            },
            {
                type: 'countdown',
                appendTo: '#col-3'
            }
        ],
        elems: {
            widgetsContainer: '#widgets-container'
        }
    });

    window.winterfell = winterfell; // TODO: comment this line if you don't needed to debug it in browser
});