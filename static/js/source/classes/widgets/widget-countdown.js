var Widgets = window.Widgets || {};

Widgets.Countdown = function(app, widget){
    this.widget = widget;
    this.$el = app.createGenericWidgetHtml(widget);

    return this.init();
};

Widgets.Countdown.prototype.init = function(){
    var that = this;

    var tpl = '<div class="title">Time is Money! <a href="javascript:void(0)"><i class="glyphicon glyphicon-plus"></i></a></div><div class="timer"></div>',
        timerTpl = '<ul>\
            <li><span class="hour"><%=hour%></span><small>Hour</small></li>\
            <li><span class="min"><%=min%></span><small>Min</small></li>\
            <li><span class="sec"><%=sec%></span><small>Sec</small></li>\
        </ul>',
        complied = _.template(timerTpl);

    that.$el.append(tpl);

    var dateStart = new Date,
        dateEnd = new Date(dateStart.getTime() + (24 * 60 * 60 * 1000));

    function checkNumberLength(num) {
        return parseInt(num) >= 10 ? num : '0' + num;
    }

    function timer() {
        var eventDate = Date.parse(dateEnd) / 1e3,
            currentDate = Math.floor(Date.now() / 1e3),
            seconds = eventDate - currentDate;

        var days, hours, minutes, seconds;

        days = Math.floor(seconds / 86400);
        seconds -= days * 60 * 60 * 24;
        hours = Math.floor(seconds / 3600);
        seconds -= hours * 60 * 60;
        minutes = Math.floor(seconds / 60);
        seconds -= minutes * 60;

        that.$el.find('.timer').html(complied({
            hour: checkNumberLength(hours),
            min: checkNumberLength(minutes),
            sec: checkNumberLength(seconds)
        }));          
    }

    timer();

    setInterval(timer, 1e3);

    return that;
};