var Widgets = window.Widgets || {};

Widgets.Mail = function(app, widget){
    this.widget = widget;
    this.$el = app.createGenericWidgetHtml(widget);

    return this.init();
};

Widgets.Mail.prototype.init = function(){
    var that = this;

    that.createStaticWidget(function(){
        that.bindActions();
    });

    return that;
};

Widgets.Mail.prototype.createStaticWidget = function(fn){
    var that = this;

    fn = fn || function(){};

    var tpl = '<div class="title">Mailbox</div>\
        <ul>\
            <li class="folder-inbox active">\
                <div class="folder-name"><i class="glyphicon glyphicon-inbox"></i> <span>Inbox</span></div>\
                <div class="badge">3</div>\
            </li>\
            <li class="folder-starred">\
                <div class="folder-name"><i class="glyphicon glyphicon-star"></i> <span>Starred</span></div>\
                <div class="badge">7</div>\
            </li>\
            <li class="folder-flagged">\
                <div class="folder-name"><i class="glyphicon glyphicon-flag"></i> <span>Flagged</span></div>\
                <div class="badge">13</div>\
            </li>\
            <li class="folder-sent">\
                <div class="folder-name"><i class="glyphicon glyphicon-send"></i> <span>Sent</span></div>\
                <div class="badge">284</div>\
            </li>\
            <li class="folder-deleted">\
                <div class="folder-name"><i class="glyphicon glyphicon-trash"></i> <span>Deleted</span></div>\
                <div class="badge">6</div>\
            </li>\
            <li class="folder-hogwarts advanced hidden">\
                <div class="folder-name"><i class="glyphicon glyphicon-tower"></i> <span>From Hogwarts</span></div>\
                <div class="badge">7</div>\
            </li>\
            <li class="toggle-hidden-items">\
                <a href="javascript:void(0)">More <i class="glyphicon glyphicon-chevron-down"></i></a>\
            </li>\
        </ul>';

    that.$el.append(tpl);

    fn();

    return that;
};

Widgets.Mail.prototype.bindActions = function(){
    var that = this;

    var $toggleAdvacedFolders = that.$el.find('.toggle-hidden-items'),
        $toggleWidgetView = that.$el.find('.title'),
        $advancedFolders = that.$el.find('.advanced');

    $toggleAdvacedFolders.on({
        click: function(){
            if ($advancedFolders.is(':hidden')) {
                $advancedFolders.removeClass('hidden');
                $toggleAdvacedFolders.find('a').html('Less <i class="glyphicon glyphicon-chevron-up"></i>');
            } else {
                $advancedFolders.addClass('hidden');
                $toggleAdvacedFolders.find('a').html('More <i class="glyphicon glyphicon-chevron-down"></i>');
            }
        }
    });

    $toggleWidgetView.on({
        click: function(){
            that.$el.toggleClass('tiny-view');
        }
    });

    return that;
};