var Widgets = window.Widgets || {};

Widgets.Tasks = function(app, widget){
    this.widget = widget;
    this.$el = app.createGenericWidgetHtml(widget);

    return this.init();
};

Widgets.Tasks.prototype.init = function(){
    var that = this;

    that.formWidgetTemplate(function(){
        that.bindActions();

        setTimeout(function(){
            that.drawChart();
            that.fillListView();
            that.fillMap();
        }, 100);
    });

    return that;
};

Widgets.Tasks.prototype.formWidgetTemplate = function(fn){
    var that = this;

    fn = fn || function(){};

    var tpl = '<div class="title">Tasks Completed</div>\
        <ul class="tabs-content">\
            <li id="tasks-chart" class="chart active"></li>\
            <li id="tasks-list" class="list"></li>\
            <li id="tasks-map" class="map"></li>\
        </ul>\
        <ul class="tabs-nav">\
            <li class="active"><a href="#tasks-chart"><i class="fa fa-pie-chart"></i><span>Chart</span></a></li>\
            <li><a href="#tasks-list"><i class="fa fa-list"></i><span>Tasks</span></a></li>\
            <li><a href="#tasks-map"><i class="fa fa-map-marker"></i><span>Events</span></a></li>\
        </ul>';

    that.$el.html(tpl);

    fn();

    return that;
};

Widgets.Tasks.prototype.fillMap = function(){
    var that = this;

    var $map = that.$el.find('.map');

    var points = [];

    _.each(that.widget.data, function(item){
        points.push(item.point.toString());
    });

    points = points.join('~');

    //$map.append('<img src="//static-maps.yandex.ru/1.x/?l=map&pt=' + points + '" alt=""/>');
    $map.css('background-image', 'url("//static-maps.yandex.ru/1.x/?l=map&pt=' + points + '")');

    return that;
};

Widgets.Tasks.prototype.fillListView = function(){
    var that = this;

    var $list = that.$el.find('.list'),
        tpl = '<li title="<%=doneText%>" class="<%=done%>"><b>&bull;</b> <%=title%></li>',
        compiled = _.template(tpl),
        $ul = $('<ul/>'),
        tasks = _.shuffle(that.widget.data);

    _.each(tasks, function(item){
        var $li = compiled({
            done: item.completed ? 'done' : 'in-progress',
            doneText:  item.completed ? 'Done' : 'In progress',
            title: item.name
        });

        $ul.append($li);
    });

    $list.append($ul);

    return that;
};

Widgets.Tasks.prototype.drawChart = function(){
    var that = this;

    var $chart = that.$el.find('.chart');

    var done = 0,
        inProgress = 0;

    _.each(that.widget.data, function(item){
        if (item.completed) {
            done++;
        } else {
            inProgress++;
        }
    });

    var percent = that.widget.data.length / 100;

    done = done / percent;
    inProgress = inProgress / percent;

    $chart.highcharts({
        chart: {
            type: 'pie'
        },
        credits: false,
        colors: ['#14b9d6', '#f0f0f0'],
        title: {
            text: done + '<sup>%</sup>',
            align: 'center',
            verticalAlign: 'middle',
            y: 10,
            style: {
                fontSize: '45px'
            }
        },
        yAxis: {
            title: {
                text: 'Total percent market share'
            }
        },
        plotOptions: {
            pie: {
                shadow: false,
                innerSize: 150
            }
        },
        tooltip: {
            formatter: function() {
                return '<b>'+ this.point.name +'</b>: '+ this.y +' %';
            }
        },
        series: [{
            name: 'Browsers',
            data: [["Done", done], ["Not yet", inProgress]],
            showInLegend: false,
            dataLabels: {
                enabled: false
            }
        }]
    });

    return that;
};

Widgets.Tasks.prototype.bindActions = function(){
    var that = this;

    var $navControls = that.$el.find('.tabs-nav a'),
        $tabs = that.$el.find('.tabs-content li');

    $navControls.on({
        click: function(e){
            e.preventDefault();

            var $this = $(this),
                $li = $this.parent();

            if ($li.hasClass('active')) {
                return false;
            }

            $navControls.parent().removeClass('active');
            $li.addClass('active');

            $tabs.hide();
            $tabs.filter($this.attr('href')).show();
        }
    });

    return that;
};