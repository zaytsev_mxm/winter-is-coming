var Widgets = window.Widgets || {};

Widgets.Events = function(app, widget){
    this.widget = widget;
    this.$el = app.createGenericWidgetHtml(widget);

    return this.init();
};

Widgets.Events.prototype.init = function(){
    var that = this;

    var tpl = '<div class="title"><i class="glyphicon glyphicon-calendar"></i> <%=date%></div>\
        <ul>\
            <li>\
                <a href="javascript:void(0)" class="place"><i class="glyphicon glyphicon-map-marker"></i> Winterfell</a>\
                <a href="javascript:void(0)" class="notes"><i class="glyphicon glyphicon-edit"></i></a>\
                <a href="javascript:void(0)" class="comments"><i class="glyphicon glyphicon-comment"></i></a>\
            </li>\
        </ul>\
        <a href="javascript:void(0)" class="btn btn-info btn-lg btn-block">add event</a>',
        compiled = _.template(tpl);

    that.$el.append(compiled({
        date: (new Date).format('F d, Y')
    }));

    return that;
};