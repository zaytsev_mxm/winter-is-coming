var Widgets = window.Widgets || {};

Widgets.Video = function(app, widget){
    this.widget = widget;
    this.$el = app.createGenericWidgetHtml(widget);

    return this.init();
};

Widgets.Video.prototype.init = function(){
    var that = this;

    that.initPlayer(function(){
        that.bindActions();
    });

    return that;
};


Widgets.Video.prototype.initPlayer = function(fn){
    var that = this;

    fn = fn || function(){};

    var tpl = '<video width="100%" poster="<%=poster%>"><source src="<%=source%>"></video>\
            <div class="toggle play"></div>\
            <div class="controls">\
                <div class="toggle play"></div>\
                <div class="progress-bar"><i></i><b></b></div>\
                <div class="sound glyphicon glyphicon-volume-up"></div>\
            </div>',
        source = _.template(tpl)(that.widget.data);

    that.$el.html(source);

    that.player = that.$el.find('video')[0];

    fn();

    return that;
};

Widgets.Video.prototype.togglePlay = function(){
    var that = this;

    var $play = that.$el.find('.toggle');

    if (that.player.currentTime <= 0 || that.player.paused || that.player.ended) {
        that.player.play();
        $play.addClass('pause').removeClass('play');
        that.$el.addClass('playing');
    } else {
        that.player.pause();
        $play.addClass('play').removeClass('pause');
        that.$el.removeClass('playing');
    }

    return that;
};

Widgets.Video.prototype.bindActions = function(){
    var that = this;

    var $play = that.$el.find('.toggle');

    $play.on({
        click: function(){
            that.togglePlay();
        }
    })

    return that;
};