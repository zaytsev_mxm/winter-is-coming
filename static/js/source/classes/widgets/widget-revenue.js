var Widgets = window.Widgets || {};

Widgets.Revenue = function(app, widget){
    this.widget = widget;
    this.$el = app.createGenericWidgetHtml(widget);

    this.monthsList = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

    return this.init();
};

Widgets.Revenue.prototype.init = function(){
    var that = this;

    that.initWidgetContent(function(){
        setTimeout(function(){
            that.drawChart();
            that.drawInquiries();
        }, 100);
    });

    return that;
};

Widgets.Revenue.prototype.initWidgetContent = function(fn){
    var that = this;

    fn = fn || function(){};

    var tpl = '<div class="chart">\
                        <div class="title">Revenue</div>\
                        <div class="chart-content"></div>\
                    </div>\
                    <div class="inquiries">\
                        <div class="left">\
                            <span>Inquiries</span>\
                            <ul class="inquiries-list"></ul>\
                        </div>\
                        <div class="right">\
                            <strong>+142</strong>\
                            <small>From last year</small>\
                        </div>\
                    </div>\
                    <ul class="gross-net">\
                        <li>\
                            <span>Gross Revenue</span>\
                            <b>9,362.74</b>\
                        </li>\
                        <li>\
                            <span>Net Revenue</span>\
                            <b>6,734.89</b>\
                        </li>\
                    </ul>\
                </div>',
        source = _.template(tpl)(that.widget.data);

    that.$el.html(source);

    that.player = that.$el.find('video')[0];

    fn();

    return that;
};

Widgets.Revenue.prototype.drawInquiries = function(){
    var that = this;

    var $container = that.$el.find('.inquiries-list');

    _.each(that.widget.data, function(item, i){
        var monthFirstChar = that.monthsList[i].substr(0,1);

        $container.append('<li><span><i class="inquiries-by-month" data-value="' + item.inquiries + '"></i></span><b>' + monthFirstChar + '</b></li>');
    });

    setTimeout(function(){
        $('.inquiries-by-month').each(function(){
            var $this = $(this),
                val = $this.data('value');

            $this.animate({
                height: val + '%'
            }).removeAttr('data-value');
        });
    }, 200);

    return that;
};

Widgets.Revenue.prototype.drawChart = function(){
    var that = this;

    var $container = that.$el.find('.chart-content');

    var revenues = [];

    _.each(that.widget.data, function(item){
        revenues.push(item.revenue);
    });

    $container.highcharts({
        chart: {
            backgroundColor: '#1fbba6',
            height: 220
        },
        credits: false,
        legend: {
            enabled: false
        },
        colors: ['#ffffff'],
        title: {
            text: '',
            style: 'display: none'
        },
        xAxis: {
            categories: that.monthsList,
            lineWidth: 0,
            tickLength: 0,
            labels: {
                style: {color: '#fff'},
                step: 2
            }
        },
        yAxis: {
            gridLineDashStyle: 'ShortDash',
            gridLineColor: '#16a693',
            title: {
                text: '',
                style: 'display: none'
            },
            stackLabels: {
                style: 'color: #ffffff'
            },
            labels: {
                style: {color: '#fff'}
            }
        },
        series: [{
            name: 'Revenue',
            data: revenues
        }]
    });    

    return that;
};