var Widgets = window.Widgets || {};

Widgets.Login = function(app, widget){
    this.widget = widget;
    this.$el = app.createGenericWidgetHtml(widget);

    return this.init();
};

Widgets.Login.prototype.init = function(){
    var that = this;

    that.createStaticWidget(function(){
        that.bindActions();
    });

    return that;
};

Widgets.Login.prototype.createStaticWidget = function(fn){
    var that = this;

    fn = fn || function(){};

    var tpl = '<form method="post" action="">\
        <div class="form-group">\
            <div class="input-group">\
                <div class="input-group-addon"><i class="glyphicon glyphicon-user"></i></div>\
                <input type="text" class="form-control input-lg" name="Username" id="login-username" placeholder="Username" autocomplete="off" />\
            </div>\
            <div class="input-group">\
                <div class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></div>\
                <input type="password" class="form-control input-lg" name="Password" placeholder="Password" />\
            </div>\
        </div>\
        <button type="submit" class="btn btn-warning btn-lg btn-block">log in</button>\
    </form>';

    that.$el.append(tpl);

    fn();

    return that;
};

Widgets.Login.prototype.bindActions = function(){
    var that = this;

    var $form = that.$el.find('form');

    $form.on({
        submit: function(e) {
            e.preventDefault();
        }
    }).validate({
        rules: {
            Username: {
                required: true,
                minlength: 3
            },
            Password: {
                required: true,
                minlength: 6
            }
        },
        messages: {
            Username: 'Username must contain at least 3 characters',
            Password: 'Password must contain at least 6 characters'
        },
        errorPlacement: function(error, element) {
            error.insertBefore(element.closest('form').find('button'));
        },
        submitHandler: function(){
            alert('Hi, ' + $.trim($('#login-username').val()) + '! Now you are logged in!');
        }
    });

    $form.find('input').on({
        focus: function(){
            $(this).parent().addClass('focused');
        },
        blur: function(){
            $(this).parent().removeClass('focused');
        }
    });

    return that;
};