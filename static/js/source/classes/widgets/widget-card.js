var Widgets = window.Widgets || {};

Widgets.Card = function(app, widget){
    this.widget = widget;
    this.$el = app.createGenericWidgetHtml(widget);

    return this.init();
};

Widgets.Card.prototype.init = function(){
    var that = this;

    var tpl = '<div class="card">\
        <img src="/static/img/arya.jpg" alt="" />\
        <span>Arya Stark <small>Winterfell</small></span>\
    </div>\
    <ul>\
        <li class="likes"><i class="glyphicon glyphicon-heart"></i><small>2,719</small></li>\
        <li class="friends"><i class="glyphicon glyphicon-user"></i><small>5,386</small></li>\
        <li class="visits"><i class="glyphicon glyphicon-eye-open"></i><small>24,953</small></li>\
    </ul>';

    that.$el.append(tpl);

    return that;
};