var Winterfell = function(options){
    var defaults = {
        widgets: [],
        widgetsInstances: [],
        elems: {
            widgetsContainer: ''
        },
        onLoad: function(){}
    };

    $.extend(true, this, defaults, options);

    this.init();
};

Winterfell.prototype.init = function(){
    var that = this;

    that.loadWidgets(that.onLoad);

    return that;
};

Winterfell.prototype.createGenericWidgetHtml = function(widget){
    var $el = $('<div class="widget widget-' + widget.type + ' ' + (widget.additionalClasses || '') + '"/>');

    // ...

    return $el;
};

Winterfell.prototype.loadWidgets = function(fn){
    var that = this;

    fn = fn || function(){};

    _.each(that.widgets, function(widget){
        that.initWidget(widget);
    });

    return that;
};

Winterfell.prototype.initWidget = function(widget){
    var that = this;

    var type = widget.type && widget.type.capitalize() || null;

    if (type && Widgets[type]) {
        var widgetInstance = new Widgets[type](that, widget),
            $toAppend = $('<div class="col-md-4"/>').wrapInner(widgetInstance.$el);

        that.widgetsInstances.push(widgetInstance);

        if (!widget.appendTo) {
            $(that.elems.widgetsContainer).append($toAppend);    
        } else {
            $(widget.appendTo).append(widgetInstance.$el);
        }
    } else {
        if (window.console && window.console.warn) {
            console.warn('A type of widget ("' + type + '") is unknown, widget was not added.');
        }
    }

    return that;
};