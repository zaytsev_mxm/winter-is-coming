module.exports = function(grunt) {
    require('jit-grunt')(grunt);

    grunt.initConfig({
        less: {
            site: {
                options: {
                    compress: false,
                    yuicompress: false,
                    optimization: 2
                },
                files: {
                    "css/style.css": "less/style.less"
                }
            },
            siteMin: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    "css/style.min.css": "less/style.less"
                }
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            libs: {
                src: ['js/source/libs/*.js'],
                dest: 'js/libs.js'
            },
            site: {
                src: ['js/source/commons.js', 'js/source/plugins/**/*.js', 'js/source/classes/widgets/*.js', 'js/source/classes/*.js', 'js/source/site.js'],
                dest: 'js/scripts.js'
            }
        },
        uglify: {
            libs: {
                files: {
                    'js/libs.min.js': ['js/libs.js']
                }
            },
            site: {
                files: {
                    'js/scripts.min.js': ['js/scripts.js']
                }
            }
        },
        watch: {
            stylesSite: {
                files: ['less/**/*.less'],
                tasks: ['less:site', 'less:siteMin'],
                options: {
                    nospawn: true
                }
            },
            scriptsLibs: {
                files: ['js/source/libs/**/*.js'],
                tasks: ['concat:libs', 'uglify:libs'],
                options: {
                    nospawn: true
                }
            },
            scriptsSite: {
                files: ['js/source/**/*.js'],
                tasks: ['concat:site', 'uglify:site'],
                options: {
                    nospawn: true
                }
            }
        }
    });

    grunt.registerTask('default', ['less', 'concat', 'uglify', 'watch']);
};